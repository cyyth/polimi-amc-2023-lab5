function [x_bar, u_bar] = compute_equilibrium(h1_ref, tt, g)
    % compute_equilibrium
    % Compute the equilibrium (x_bar, u_bar) such that h1_bar = h1_ref

    v = [sqrt(2*g)*(tt.a2/tt.S1) tt.gamma/tt.S1 ;
        sqrt(2*g)*(-tt.a2/tt.S2) (1-tt.gamma)/tt.S2]\[sqrt(2*h1_ref*g)*(tt.a1/tt.S1) ; 0];
    x_bar = [h1_ref ; v(1)^2];
    u_bar = v(2);
end

