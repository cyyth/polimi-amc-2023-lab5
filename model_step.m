function [ xkp1, yk ] = model_step(xk, uk, h, tt, g, method)
    % model_step
    % Function implementing the discrete-time evolution of the system state x(k+1) and output y(k).
    % Obtained discretizing the system dynamics.
    % Methods available: "euler", "rk4"
    %
    % x(k+1) = f(x(k), u(k))
    % y(k) = x(k)
    
    
    if method == "euler"
        % Discretization via Forward Euler
        fx = [(-tt.a1/tt.S1)*sqrt(2*g*xk(1))+(tt.a2/tt.S1)*sqrt(2*g*xk(2))+tt.gamma/tt.S1*uk ; 
        (-tt.a2/tt.S2)*sqrt(2*g*xk(2))+(1-tt.gamma)/tt.S2*uk];
        xkp1 = xk + h.*fx;
        yk = xk;
    elseif method == "rk4"
        k1 = [(-tt.a1/tt.S1)*sqrt(2*g*xk(1))+(tt.a2/tt.S1)*sqrt(2*g*xk(2))+tt.gamma/tt.S1*uk ; 
        (-tt.a2/tt.S2)*sqrt(2*g*xk(2))+(1-tt.gamma)/tt.S2*uk];
        k2 = [(-tt.a1/tt.S1)*sqrt(2*g*(xk(1)+0.5*h*k1(1)))+(tt.a2/tt.S1)*sqrt(2*g*(xk(2)+0.5*h*k1(2)))+tt.gamma/tt.S1*uk ; 
        (-tt.a2/tt.S2)*sqrt(2*g*(xk(2)+0.5*h*k1(2)))+(1-tt.gamma)/tt.S2*uk];
        k3 = [(-tt.a1/tt.S1)*sqrt(2*g*(xk(1)+0.5*h*k2(1)))+(tt.a2/tt.S1)*sqrt(2*g*(xk(2)+0.5*h*k2(2)))+tt.gamma/tt.S1*uk ; 
        (-tt.a2/tt.S2)*sqrt(2*g*(xk(2)+0.5*h*k2(2)))+(1-tt.gamma)/tt.S2*uk];
        k4 = [(-tt.a1/tt.S1)*sqrt(2*g*(xk(1)+h*k3(1)))+(tt.a2/tt.S1)*sqrt(2*g*(xk(2)+h*k3(2)))+tt.gamma/tt.S1*uk ; 
        (-tt.a2/tt.S2)*sqrt(2*g*(xk(2)+h*k3(2)))+(1-tt.gamma)/tt.S2*uk];
        xkp1 = xk+h/6*(k1+2*k2+2*k3+k4);
        yk = xk;
    end
end