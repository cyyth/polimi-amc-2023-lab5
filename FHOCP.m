function [u_opt, x_opt] = FHOCP(xk, Q, R, N, x_bar, u_bar, tau_s, tt, g, method, u_rate_lim, ukm1)
    % FHCOP
    % Solve the Finite Horizon Optimal Control Problem (FHOCP)
    %
    % Arguments
    % - xk: The current state
    % - Q: The state weight
    % - R: The input weight
    % - Np: The prediction horizon
    % - x_bar: The target state equilibrium
    % - u_bar: The target input equilibrium
    % - Ts: Discretization time-step
    %
    % Returns
    % - u_opt: The optimal control move at the current time step (u^*(k))
       
    % Import the CasADi toolkit and instantiate the optimization problem (opti)
    import casadi.*;
    opti = casadi.Opti();
    
    u_min = 1e-4; u_max = 1e-3;
    
    x_min = [0.1 ; 0.1]; x_max = [1.3 ; 1.2];

    qrlim = 0.5*1e-5;

    %%%%%% Declare the optimization variables %%%%%%
    u = opti.variable(1, N); %[1-N]
    x = opti.variable(2, N); %[2-N+1]
    
    
    %%%%%% Impose the constraints %%%%%%
    
    % Initial state constraint: x(k) == xk
    opti.subject_to(x(:, 1) == xk);

    % input rate constrants (also depends on the previous uk)
    if u_rate_lim
        opti.subject_to(u(1)^2 <= (ukm1+qrlim*tau_s)^2); % uk-1
        for i = 1:N-1
            opti.subject_to((u(i+1)-u(i))^2 <= (qrlim*tau_s)^2); %uk -- uk+N-1
        end
    end

    % Input constraints:  u_min <= u(k+i) <= u_max
    for i = 1:N
        opti.subject_to(u_min <= u(i) <= u_max);
    end
    
    for i = 1:N-1
        % State constraints:  x_min <= x(k+i) <= x_max   $forall
        opti.subject_to(x_min <= x(:,i) <= x_max);
        % Set the system dynamics as a constraint:  x(k+i+1) == f(x(k), u(k))
        [xip1, ~] = model_step(x(:,i), u(i), tau_s, tt, g, method);
        opti.subject_to(x(:, i+1) == xip1)
    end

    % Terminal constraint: x(k+N+1) = x_bar
    opti.subject_to(x(1, end) == x_bar(1));
    
    %%%%%% System dynamics and cost function %%%%%%
    
    % Cost function initialization
    J = 0;
    
    for jj=1:N      % Note that the MATLAB index jj = 1 corresponds to i = 0
        % Add the i-th term to the cost function: (u(k+i)-u_bar)' * R * (u(k+i)-u_bar) + (x(k+i)-x_bar)' * Q * (x(k+i)-x_bar)
        J = J + (x(:,jj)-x_bar)' * Q * (x(:,jj)-x_bar) + (u(jj)-u_bar)' * R * (u(jj)-u_bar);
    end
    
    %%%%%% Set the initial guess of the optimization variables %%%%%%
    
    % Possible initial guess for x:    x(k+i) = xk  ∀i = 0, ..., N
    opti.set_initial(x, x_bar.*ones(2,N));
    
    % Possible initial guess for u:    u(k+i) = u_min  ∀i = 0, ..., N-1
    opti.set_initial(u, u_max.*ones(1,N));
    
    
    
    %%%%%% CasADi Settings (do not change) %%%%%%
    % Declare the cost function
    opti.minimize(J);
    
    % Options of the optimization problem
    prob_opts = struct;
    prob_opts.expand = true;
    prob_opts.ipopt.print_level = 0;    % Disable printing
    prob_opts.print_time = false;       % Do not print the timestamp
    
    % Options of the solver
    ip_opts = struct;
    ip_opts.print_level = 0;            % Disable printing
    ip_opts.max_iter = 1e5;             % Maximum iterations
    ip_opts.compl_inf_tol = 1e-6;
    
    % Set the solver
    opti.solver('ipopt', prob_opts, ip_opts);
    
    
    %%%%%% Solve the optimization problem %%%%%%
    try
        sol = opti.solve();
        
        % Extract the optimal control action
        u_opt = sol.value(u);
        x_opt = sol.value(x);
        
    catch EX
        keyboard; % Enter the debug mode
    end
end