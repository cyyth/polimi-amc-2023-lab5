classdef MPC < matlab.System
    % MPC Model Predictive Control Block

    % Define the properties of the block, that can be changed 
    properties(Nontunable)
        % N Prediction Horizon
        N = 20;
        % Q Q Matrix
        Q = [1 0 ; 0 1];
        % R R Matrix
        R = 1;
        % tau_s Sampling Time
        tau_s = 1;
        % tt Tank Properties' Struct
        tt = [];
        % g Gravitational Acceleration
        g = 9.81;
        % integration_method Integration method: rk4 or euler
        integration_method = "euler";
        % REN Enable input rate limiter
        REN = false;
    end
    
     % Define here the states of the block, if any. 
     % States are only necessary if the block needs to keep memory of past data.
    properties(DiscreteState)
        ukm1;
    end
    
    

    methods(Access = protected)
        
        function [u_opt, x_bar, u_bar] = stepImpl(obj, h1_ref, xk)
            % Given h1_ref, compute (x_bar, u_bar)
            [x_bar, u_bar] = compute_equilibrium(h1_ref, obj.tt, obj.g);
            % Solve the FHOCP
            [U_opt, ~] = FHOCP(xk, obj.Q, obj.R, obj.N, x_bar, u_bar, obj.tau_s, obj.tt, obj.g, obj.integration_method, obj.REN, obj.ukm1);
            u_opt = U_opt(1);
            obj.ukm1 = u_opt; % for limiting the rate of input signal
            % Update the states of the block, if any
        end


        function n_in = getNumInputsImpl(~)
            % Define total number of input signals of the block
            n_in = 2;
        end

        function n_out = getNumOutputsImpl(~)
            % Define total number of output signal of the block
            n_out = 3;
        end

        function [os1, os2, os3] = getOutputSizeImpl(~)
            % Return the size of the output signal
            os1 = 1; os2 = 2; os3 = 1;
        end

        function [ot1, ot2, ot3] = getOutputDataTypeImpl(~)
            % Return data type of the output port. 
            % Choose between MATLAB Data types (e.g. "double", "int32", "uint64", etc.)
            ot1 = "double"; ot2 = "double"; ot3 = "double"; 
        end

        function [oc1, oc2, oc3] = isOutputComplexImpl(~)
            % Return true if the output signal is complex, false otherwise
            oc1 = false; oc2 = false; oc3 = false;
        end

        function [of1, of2, of3] = isOutputFixedSizeImpl(~)
            % Return true if the output signal has a fixed size, false otherwise
             of1 = true; of2 = true; of3 = true;
        end


        function sts = getSampleTimeImpl(obj)
            % Define the sampling time of the block.
            % This is a discrete-time block with sampling time specified by the user via the tau_s parameter
            sts = obj.createSampleTime("Type", "Discrete", "SampleTime", obj.tau_s);
        end

        
        %%%%%% States methods %%%%%%
        % Leave commented if there is no state
        
        function [sz,dt,cp] = getDiscreteStateSpecificationImpl(obj,name)
            % Return size, data type, and complexity of discrete-state specified in name
            if strcmp(name, 'ukm1')
                sz = 1;          % Size of State1
                dt = "double";           % Type of State 1
                cp = 0;           % Complexity of State1
            end
        end
        
        
        function resetImpl(obj)
            % Initialize the states at the beginning of the simulation
            obj.ukm1 = 0;
        end
    end
end
