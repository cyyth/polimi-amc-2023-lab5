function [A, B, C, D] = linearize(h1_ref, tt, g)
%LINEARIZE Summary of this function goes here
%   Detailed explanation goes here
    [x_bar, ~] = compute_equilibrium(h1_ref, tt, g);
    A = [(-tt.a1/2/tt.S1)*sqrt(2*g/x_bar(1)) (tt.a2/2/tt.S1)*sqrt(2*g/x_bar(2)) ; 0 (-tt.a2/2/tt.S2)*sqrt(2*g/x_bar(2))];
    B = [tt.gamma/tt.S1 ; (1-tt.gamma)/tt.S2];
    C = eye(2);
    m = size(B,2); p = size(C,1);
    D = zeros(p, m);
end

