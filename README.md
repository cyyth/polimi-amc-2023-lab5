# Laboratory 5 – Model Predictive Control of a Two-Tank System
The last laboratory of «Advanced and Multivariable Control» (AMC) A.Y. 2022/2023
This laboratory requires CadADi to be installed.



# Included Contents
### Live Scripts:
- [main.mlx](main.mlx)

### Simulink Models:
- [ttsys_lqr.slx](ttsys_lqr.slx)
![alt text](figures/ttsys_lqr.jpeg)

- [ttsys_mpc.slx](ttsys_mpc.slx)
![alt text](figures/ttsys_mpc.jpeg)

- [two_tanks_sys.slx](two_tanks_sys.slx)
![alt text](figures/two_tanks_sys.jpeg)